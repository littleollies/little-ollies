Little Ollies was founded according to Feng Shui (The flow of energy).
Our interior design and decoration are in sync with the energy flow of nature.
Our dishes combine the best of the East and the West to bring the right balance to the body and the mind.

Address: 308 S Hunter St, Aspen, CO 81611, USA

Phone: 970-544-9888

Website: [http://www.littleolliesaspen.com](http://www.littleolliesaspen.com)
